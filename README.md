# Edie Homepage #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://frosty-wright-741225.netlify.app/) | [Solution](https://devchallenges.io/solutions/58m9BHqxpQFnDgWUsvyK) | [Challenge](https://devchallenges.io/challenges/xobQBuf8zWWmiYMIAZe0) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FEdieHomageThumbnail.png%3Falt%3Dmedia%26token%3D72c573b5-6389-425c-b947-de63f0f5b2ef&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/xobQBuf8zWWmiYMIAZe0) was to build an application to complete the given user stories.

## Built With

- [HTML](https://html.spec.whatwg.org/)
- [CSS](https://www.w3.org/Style/CSS/Overview.en.html)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

## Features

This project features a responsive page that goes from a small mobile (320px width) view up to a desktop (4K) view, including a hamburger menu on mobile screens.

## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
