const hamburger = document.getElementById('hamburger');
const navUl = document.getElementById('nav-ul');

hamburger.addEventListener('click', () => {
  switch (hamburger.innerHTML) {
    case "menu":
      hamburger.innerHTML = "close";
      break;
    case "close":
      hamburger.innerHTML = "menu";
      break;

    default:
      hamburger.innerHTML = "menu";
      break;
  }

  navUl.classList.toggle('show');
})